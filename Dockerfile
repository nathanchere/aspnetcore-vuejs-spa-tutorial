# Build

FROM mcr.microsoft.com/dotnet/core/sdk:3.0.100-preview7 AS build-env
WORKDIR /app

COPY . .
RUN dotnet publish -c Release -o ./dist NCCV.Web

# DO VUE STUFF
# FROM node:8.9.4 as clientBuild
# WORKDIR /ClientApp
# COPY src/SampleSpa/ClientApp/ .
# RUN npm install
# RUN npm run build

#  Run 

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0.100-preview7
WORKDIR /app
EXPOSE 80 81

COPY --from=build-env /app/dist .
ENTRYPOINT ["dotnet", "NCCV.Web.dll"]
